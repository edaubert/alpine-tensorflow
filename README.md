Alpine Tensorflow
-----------------

This fork of [better/alpine-tensorflow](https://github.com/better/alpine-tensorflow) aims to provide a Dockerfile to build a tensorflow image.

Currently there is only a image based on Alpine edge and Tensorflow 1.8.0.
